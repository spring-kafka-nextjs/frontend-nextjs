import '@/app/globals.css'
import React, { useState } from 'react';
import { useRouter } from 'next/router';

import ContentToAlert from "@/component/ContentToAlert";
import ReconnectingWebSocket from "reconnecting-websocket";

const WS_ENDPOINT = 'ws://localhost:8827/ws';
let ws: ReconnectingWebSocket | null = null;

const Register = () => {
    const router = useRouter();
    const [usernameError, setUsernameError] = useState<string | null>(null);
    const [emailError, setEmailError] = useState<string | null>(null);
    const [passwordError, setPasswordError] = useState<string | null>(null);
    const [confirmPasswordError, setConfirmPasswordError] = useState<string | null>(null);
    const [inputValid, setInputValid] = useState(false);
    const [alert, setAlert] = useState<{ content: string, severity: 'success' | 'error' | 'info' | 'warning' } | null>(null);
    const [visible, setVisible] = useState(false);
    const [formData, setFormData] = useState({
        username: '',
        email: '',
        password: '',
        confirmPassword: '',
        role: 'USER',
    });

    const [warnings, setWarnings] = useState({
        usernameEmpty: false,
        usernameLength: false,
        usernameSymbol: false,
        email: false,
    });

    const isValidEmail = (email: string) => {
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return regex.test(email);
    };
    const containsSymbol = (string: string) => {
        const regex = /[^a-zA-Z0-9]/;
        return regex.test(string);
    };
    const evaluatePasswordStrength = (password: any) => {
        if (!password) return 0;

        let strength = 0;
        if (password.length > 7) strength++;
        if (password.match(/[A-Z]/)) strength++;
        if (password.match(/[a-z]/)) strength++;
        if (password.match(/[0-9]/)) strength++;
        if (password.match(/[^a-zA-Z0-9]/)) strength++;

        return strength;
    };
    const passwordStrength = evaluatePasswordStrength(formData.password);
    const getStrengthColor = () => {
        if (passwordStrength <= 1) return 'bg-red-600 ';
        if (passwordStrength > 1 && passwordStrength <= 3) return 'bg-yellow-600';
        return 'bg-green-600';
    };
    const getStrengthTextColor = () => {
        if (!formData.password) return "hidden";
        if (passwordStrength === 1) return "text-red-600";
        if (passwordStrength > 1 && passwordStrength <= 3) return "text-yellow-600";
        return "text-green-600";
    };


// Handler
    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setFormData(prev => ({
            ...prev,
            [name]: value,
        }));

        if (name === 'confirmPassword') {
            if (value !== formData.password) {
                setConfirmPasswordError("Passwords do not match.");
            } else {
                setConfirmPasswordError(null);
            }
        }


        if (name === 'password' && formData.confirmPassword && formData.confirmPassword !== value) {
            setConfirmPasswordError("Passwords do not match.");
        }



        if (name === 'username') {
            setWarnings(prev => ({
                ...prev,
                usernameEmpty: value === '',
                usernameLength: value.length > 16,
                usernameSymbol: containsSymbol(value),
            }));
            setUsernameError(null);
        } else if (name === 'email'){
            setEmailError(null);
        } else if (name === 'password' && formData.confirmPassword === value) {
            setConfirmPasswordError(null);
        } else {
            setWarnings(prev => ({
                ...prev,
                [name]: false,
            }));
            setPasswordError(null)
        }
    };
    const handleWebSocketValidation = (endpoint: string, data: string, setError: (error: string | null) => void) => {
        if (ws) {
            ws.close();
            ws = null;
        }

        ws = new ReconnectingWebSocket(`${WS_ENDPOINT}/${endpoint}`);
        ws.onopen = () => {
            ws?.send(data);
        };

        ws.onmessage = (event) => {
            if (event.data.includes("already taken")) {
                setError(event.data);
                setInputValid(false)
            } else if (event.data === "Email is available.") {
                setError(null);
                setInputValid(true)
            } else if (event.data === "Username is available.") {
                setError(null);
                setInputValid(true)
            } else {
                setError(event.data);
                setInputValid(false)
            }
            ws?.close();
        };

        ws.onerror = () => {
            setError(`Failed to validate ${endpoint.split('-')[1]}. Try again later.`);
            ws?.close();
        };
    };

    const handleUsernameBlur = () => {
        if (formData.username.trim() !== '') {
            handleWebSocketValidation('validate-username', formData.username, setUsernameError);
        }
    };

    const handleEmailBlur = () => {
        if (formData.email.trim() !== '') {
            if (isValidEmail(formData.email)){
                handleWebSocketValidation('validate-email', formData.email, setEmailError);
            } else {
                setEmailError('Please enter a valid email.')
            }

        }
    };
    const handleSubmit = async (e: React.FormEvent) => {
        e.preventDefault();

        const usernameEmpty = formData.username === '';
        const usernameLength = formData.username.length > 16;
        const usernameSymbol = containsSymbol(formData.username);

        if (usernameEmpty || usernameLength || usernameSymbol || formData.email === '' || !isValidEmail(formData.email)) {
            setWarnings({
                usernameEmpty,
                usernameLength,
                usernameSymbol,
                email: formData.email === '' || !isValidEmail(formData.email),
            });
            return;
        }

        if (formData.password !== formData.confirmPassword) {
            setConfirmPasswordError("Passwords do not match.");
            return;
        }

        const user_agent = navigator.userAgent;

        const payload = {
            ...formData,
            user_agent,
        };

        if (passwordStrength <= 1){
            setPasswordError('Your password weak.')
        } else{
            if (inputValid && !usernameError && !emailError && !passwordError && !confirmPasswordError){
                try {
                    const response = await fetch('/api/produce-register', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(payload),
                    });

                    const data = await response.json();

                    if (data.success) {
                        setAlert({
                            content: "Successfully registered!",
                            severity: "success"
                        });
                        setVisible(true);
                        setTimeout(() => {
                            router.push('/');
                        }, 3000);
                    } else {
                        setAlert({
                            content: "Error during registration!",
                            severity: "warning"
                        });
                        setVisible(true);
                    }
                } catch (error) {
                    setAlert({
                        content: "Failed to send data.",
                        severity: "error"
                    });
                    setVisible(true);
                }
            } else {
                setAlert({
                    content: "Please check your data",
                    severity: "warning"
                });
                setVisible(true);
            }
        }
    };

    return (
        <>
            <div className="min-h-screen flex flex-col justify-center items-center bg-gradient-to-br from-gray-100 to-gray-200 font-poppins">
                <div className="p-10 bg-white rounded-lg shadow-xl w-96 space-y-6">
                    <h1 className="text-2xl text-center font-semibold mb-4 text-gray-800">User Registration</h1>

                    <form onSubmit={handleSubmit}>
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2">Username</label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:ring focus:ring-blue-200"
                                type="text"
                                name="username"
                                placeholder="Enter your username"
                                value={formData.username}
                                onChange={handleInputChange}
                                onBlur={handleUsernameBlur}
                            />
                            {warnings.usernameEmpty && <p className="text-red-500 text-xs mt-1">Please enter a username.</p>}
                            {warnings.usernameLength && <p className="text-red-500 text-xs mt-1">Username must not exceed 16 characters.</p>}
                            {warnings.usernameSymbol && <p className="text-red-500 text-xs mt-1">Username must not contain symbols.</p>}
                            {usernameError && <p className="text-red-500 text-xs mt-1">{usernameError}</p>}
                        </div>
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2">Email</label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:ring focus:ring-blue-200"
                                type="text"
                                name="email"
                                placeholder="Enter your email"
                                value={formData.email}
                                onChange={handleInputChange}
                                onBlur={handleEmailBlur}
                            />
                            {warnings.email && <p className="text-red-500 text-xs mt-1">{isValidEmail(formData.email) ? "Please enter an email." : "Please enter a valid email."}</p>}
                            {emailError && <p className="text-red-500 text-xs mt-1">{emailError}</p>}
                        </div>
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2">Password</label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:ring focus:ring-blue-200"
                                type="password"
                                name="password"
                                placeholder="Enter your password"
                                value={formData.password}
                                onChange={handleInputChange}
                            />
                            {passwordError && <p className="text-red-500 text-xs mt-1">{passwordError}</p>}
                            <div className="mt-2">
                                <div className="h-2 bg-gray-300 rounded">
                                    <div className={`h-full ${getStrengthColor()} rounded`} style={{ width: `${(passwordStrength / 5) * 100}%` }}></div>
                                </div>
                                <p className={`text-xs mt-1 ${getStrengthTextColor()}`}>
                                    {passwordStrength <= 1 && "Weak"}
                                    {passwordStrength > 1 && passwordStrength <= 3 && "Average"}
                                    {passwordStrength > 3 && "Strong"}
                                </p>
                            </div>
                        </div>
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2">Confirm Password</label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:ring focus:ring-blue-200"
                                type="password"
                                name="confirmPassword"
                                placeholder="Re-enter your password"
                                value={formData.confirmPassword}
                                onChange={handleInputChange}
                            />
                            {confirmPasswordError && <p className="text-red-500 text-xs mt-1">{confirmPasswordError}</p>}
                        </div>


                        <div>
                            <button
                                type="submit"
                                className="w-full bg-blue-500 text-white py-3 rounded-lg transition duration-300 transform hover:bg-blue-600 hover:scale-105 focus:outline-none focus:ring-2 focus:ring-blue-400 focus:ring-opacity-50"
                            >
                                Register
                            </button>
                        </div>
                    </form>
                </div>

                {alert && <ContentToAlert severity={alert.severity} content={alert.content} isVisible={visible} onClose={() => setAlert(null)}/>}
            </div>
        </>
    );
};

export default Register;
