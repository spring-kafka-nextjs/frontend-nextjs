import type { NextApiRequest, NextApiResponse } from 'next';
import { produceRegister } from '@/app/utils/kafkaProducer';

export default async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method === 'POST') {
        const data = req.body;

        let ip_address = req.headers['x-forwarded-for'] || req.socket.remoteAddress || '';
        if (Array.isArray(ip_address)) {
            ip_address = ip_address[0];
        }
        if (ip_address === '::1') {
            ip_address = '127.0.0.1';
        }

        const kafkaPayload = {
            ...data,
            ip_address,
        };
        await produceRegister('register-user', JSON.stringify(kafkaPayload)).then(()=>{
            res.status(200).json({ success: true, message: "Successfully Register" });
        }).catch((error) => {
            res.status(400).json({ success: false, message: error });
        })
    } else {
        res.status(405).end();
    }
};