import '@/app/globals.css'
import {useRouter} from 'next/router';
import React, {useEffect, useState} from 'react';
import ReconnectingWebSocket from "reconnecting-websocket"
import ContentToAlert from "@/component/ContentToAlert";


const Confirmation = () => {
    const router = useRouter();
    const [token, setToken] = useState<string | null>(null);
    const [username, setUsername] = useState<string | null>(null);
    const [ws, setWs] = useState<ReconnectingWebSocket | null>(null);
    const [wsResendToken, setWsResendToken] = useState<ReconnectingWebSocket | null>(null);
    const [alert, setAlert] = useState<{ content: string, severity: 'success' | 'error' | 'info' | 'warning' } | null>(null);
    const [visible, setVisible] = useState(false);
    const [loadingResend, setLoadingResend] = useState(false);
    const [captchaValue, setCaptchaValue] = useState<string | null>(null);
    const [captchaQuestion, setCaptchaQuestion] = useState<string | null>(null);
    const [expectedAnswer, setExpectedAnswer] = useState<number | null>(null);
    const [isCaptchaCorrect, setIsCaptchaCorrect] = useState<boolean>(false);
    const [showCaptcha, setShowCaptcha] = useState<boolean>(false);

    const generateCaptcha = () => {
        const num1 = Math.floor(Math.random() * 10) + 1;
        const num2 = Math.floor(Math.random() * 10) + 1;
        setCaptchaQuestion(`${num1} + ${num2} = ?`);
        setExpectedAnswer(num1 + num2);
    }

    const handleCaptchaInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        setCaptchaValue(e.target.value);
    }

    const verifyCaptcha = () => {
        if (parseInt(captchaValue as string) === expectedAnswer) {
            setIsCaptchaCorrect(true);
            setShowCaptcha(false);

            handleResendActivation().then((data) => {
                console.log(data);
            });

        } else {
            setIsCaptchaCorrect(false);
            setAlert({
                content: "Captcha answer is incorrect.",
                severity: "error"
            });
            setVisible(true);
            generateCaptcha();
        }
    }



    useEffect(() => {
        if (router.query.token) {
            setToken(router.query.token as string);
        }
        if (router.query.username) {
            setUsername(router.query.username as string);
        }

        generateCaptcha();

        const ws = new ReconnectingWebSocket('ws://localhost:8827/ws/register');
        ws.addEventListener('open', () => {
            console.log('WebSocket connection opened.');
        });
        ws.addEventListener('message', (event) => {
            if (event.data === "User Successfully Activated") {
                setAlert({
                    content: event.data,
                    severity: "info"
                });
                setVisible(true);
                setTimeout(async () => {
                    await router.push('/');
                }, 3000);
            } else {
                setAlert({
                    content: event.data,
                    severity: "info"
                });
                setVisible(true);
            }
            setAlert({
                content: event.data,
                severity: "info"
            });
            setVisible(true);
        });

        setWs(ws);

        const wsResend = new ReconnectingWebSocket('ws://localhost:8827/ws/resend-token');
        wsResend.addEventListener('open', () => {
            console.log('ResendToken WebSocket connection opened.');
        });
        wsResend.addEventListener('message', (event) => {


            if (event.data === "Email successfully sent.") {
                setTimeout(() => {
                    setLoadingResend(false);
                    setAlert({
                        content: event.data,
                        severity: "info"
                    });
                    setVisible(true);


                }, 2000);

                setTimeout(() => {
                    location.reload();
                }, 2000);
            } else {
                setAlert({
                    content: event.data,
                    severity: "info"
                });
                setVisible(true);
            }
        });

        setWsResendToken(wsResend);

        return () => {
            ws.close();
            wsResend.close();
        };
    }, [router.query]);

    const handleConfirmation = () => {
        if (token && ws) {
            ws.send(token);

        } else {
            setAlert({
                content: "Token not found or WebSocket connection not established.",
                severity: "error"
            });
            setVisible(true);
        }
    };

    const handleCaptcha = async (e: React.MouseEvent<HTMLAnchorElement>) => {
        e.preventDefault();
        setShowCaptcha(true);
    };

    const handleResendActivation = async () => {

        if (username && wsResendToken) {
            setLoadingResend(true);
            wsResendToken.send(username);
        } else {
            setAlert({
                content: "Token not found or WebSocket connection for resend not established.",
                severity: "error"
            });
            setVisible(true);
        }
    };

    return (
        <>
            <div
                className="min-h-screen flex flex-col justify-center items-center bg-gradient-to-br from-gray-100 to-gray-200 font-poppins">
                <div className="p-10 bg-white rounded-lg shadow-xl w-96 space-y-6">
                    <h1 className="text-2xl text-center font-semibold mb-4 text-gray-800">Registration Confirmation</h1>
                    {token && username ? (
                        <button
                            onClick={handleConfirmation}
                            className="w-full bg-blue-500 text-white py-3 rounded-lg transition duration-300 transform hover:bg-blue-600 hover:scale-105 focus:outline-none focus:ring-2 focus:ring-blue-400 focus:ring-opacity-50"
                        >
                            Confirm Registration
                        </button>
                    ) : (
                        <div className="flex justify-center items-center space-x-3">
                            <svg className="animate-spin h-6 w-6 text-blue-500" viewBox="0 0 24 24">
                                <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor"
                                        strokeWidth="4"></circle>
                                <path className="opacity-75" fill="currentColor"
                                      d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                            </svg>
                            <span className="text-blue-500">Loading...</span>
                        </div>
                    )}
                    <div className="flex items-center justify-center space-x-2">
                        {loadingResend && (
                            <svg className="animate-spin h-5 w-5 text-blue-500" viewBox="0 0 24 24">
                                <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor"
                                        strokeWidth="4"></circle>
                                <path className="opacity-75" fill="currentColor"
                                      d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                            </svg>
                        )}
                        {showCaptcha ? (
                            <div className="flex items-center space-x-4 text-sm text-black">
                                <span>{captchaQuestion || 'Loading...'}</span>
                                <input
                                    type="number"
                                    value={captchaValue || ''}
                                    onChange={handleCaptchaInput}
                                    placeholder="Answer"
                                    className="border px-2 py-1 rounded-md text-sm w-1/2 hide-number-spinners"
                                />
                                <button
                                    onClick={verifyCaptcha}
                                    className="bg-blue-500 text-white px-2 py-1 rounded-md hover:bg-blue-600 transition duration-200"
                                >
                                    Verify
                                </button>
                            </div>
                        ) : (
                            <a
                                href="#"
                                onClick={handleCaptcha}
                                className="text-center text-blue-500 cursor-pointer hover:underline hover:text-blue-600"
                            >
                                Resend Activation Email
                            </a>
                        )}
                    </div>
                </div>
                {alert && <ContentToAlert severity={alert.severity} content={alert.content} isVisible={visible}
                                          onClose={() => setAlert(null)}/>}
            </div>
        </>


    );
};

export default Confirmation;