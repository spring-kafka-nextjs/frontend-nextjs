import React, { useState, useEffect } from 'react';
import Alert, { AlertColor } from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Stack from '@mui/material/Stack';

interface AlertProps {
    content: string;
    severity: AlertColor;
    isVisible: boolean;
    onClose: () => void;
}

const ContentToAlert: React.FC<AlertProps> = ({ severity, content, isVisible, onClose }) => {
    const [visible, setVisible] = useState(isVisible);

    let title: string;
    switch (severity) {
        case "success":
            title = "Success";
            break;
        case "info":
            title = "Info";
            break;
        case "error":
            title = "Error";
            break;
        case "warning":
            title = "Warning";
            break;
    }

    useEffect(() => {
        setVisible(isVisible);
    }, [isVisible]);

    const handleClose = () => {
        setVisible(false);
        setTimeout(() => {
            if (onClose) {
                onClose();
            }
        }, 300);
    };

    if (!visible) {
        return null;
    }

    return (
        <div style={{
            width: '30%',
            position: 'fixed',
            bottom: '20px',
            right: '20px',
            zIndex: 1000,
            opacity: visible ? 1 : 0,
            transition: 'opacity 0.3s ease-out'
        }}>
            <Stack sx={{ width: '100%' }} spacing={2}>
                <Alert onClose={handleClose} severity={severity}>
                    <AlertTitle>{title}</AlertTitle>
                    {content}
                </Alert>
            </Stack>
        </div>
    );
};

export default ContentToAlert;
