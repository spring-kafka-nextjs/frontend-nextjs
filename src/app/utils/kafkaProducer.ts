import { Kafka } from 'kafkajs';

const kafka = new Kafka({
    clientId: 'user-service',
    brokers: ['localhost:9092'],
});

export const produceRegister = async (topic: string, message: string) => {
    const producer = kafka.producer();
    await producer.connect()

    await producer.send({
        topic,
        messages: [{ value: message }],
    });

    await producer.disconnect();
};